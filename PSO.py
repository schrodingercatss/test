import numpy as np
import matplotlib.pyplot as plt
import subprocess
import time
import psutil


dim = 3
num_particles = 2
num_iters = 100
x_limits = [1, 1.2]
v_limits = [-0.01, 0.01]

def is_process_running(process_name):
    """
    Check if there is any running process that contains the given name.
    :param process_name: Name of the process to look for.
    :return: True if a process with the given name is found, otherwise False.
    """
    for proc in psutil.process_iter(['pid', 'name']):
        try:
            # Check if process name matches
            if process_name.lower() in proc.info['name'].lower():
                return True
        except (psutil.NoSuchProcess, psutil.AccessDenied, psutil.ZombieProcess):
            pass
    return False

def wait_for_process_to_finish(process_name, check_interval=5):
    """
    Wait until the specified process is no longer running.
    :param process_name: Name of the process to wait for.
    :param check_interval: Time interval between checks (in seconds).
    """
    while is_process_running(process_name):
        # print(f"Process '{process_name}' is still running. Waiting...")
        time.sleep(check_interval)
    # print(f"Process '{process_name}' has finished.")



def write_particle_data(filename, particles):
    with open(filename, "w") as f:
        for p in particles:
            line = " ".join(map(str, p.position))
            f.write(line + "\n")

def read_simulation_scores(filename):
    scores = []
    with open(filename, "r") as f:
        for line in f:
            scores.append(float(line.strip()))
    return scores

def run_simulation_and_collect_results(num_particles):
    process1 = subprocess.Popen(["perl", "run_all.pl", f"--num_simulations={num_particles}", f"--num_dimensions={dim}"])
    process1.communicate()
    wait_for_process_to_finish("simv_random", check_interval=5)

    process2 = subprocess.Popen(["perl", "rst_collect.pl"])

    process2.communicate()



def objective_function_with_simulation(particles):
    write_particle_data("from_PSO.data", particles)
    run_simulation_and_collect_results(num_particles)
    scores = read_simulation_scores("to_PSO.data")
    return scores

class Particle:
    def __init__(self, dim, x_limits, v_limits, initial_position=None):
        if initial_position is not None:
            self.position = np.array(initial_position)
        else:
            self.position = np.random.uniform(x_limits[0], x_limits[1], dim)
        self.velocity = np.random.uniform(v_limits[0], v_limits[1], dim)
        self.best_position = self.position
        self.best_value = np.inf

    def update_velocity(self, global_best, w, c1, c2, v_limits):
        r1, r2 = np.random.rand(2)
        inertia = w * self.velocity
        cognitive = c1 * r1 * (self.best_position - self.position)
        social = c2 * r2 * (global_best - self.position)
        self.velocity = inertia + cognitive + social
        self.velocity = np.clip(self.velocity, v_limits[0], v_limits[1])

    def update_position(self, x_limits):
        self.position += self.velocity
        self.position = np.clip(self.position, x_limits[0], x_limits[1])


def PSO(num_particles, dim, num_iters, x_limits, v_limits, w=0.8, c1=0.5, c2=0.5):
    """
        num_particles: 粒子数
        dim: 维度
        num_iters: 迭代轮数
        x_limits: x的范围
        v_limits: 粒子的运动速度
        w: 惯性权重, 越大搜索范围越大, 一般是0.4-0.9
        c1: 认知学习因子
        c2: 社会学习因子
    """
    particles = [Particle(dim, x_limits, v_limits) for _ in range(num_particles - 1)]
    particles.append(Particle(dim, x_limits, v_limits, initial_position=[1] * dim))
    
    global_best = min(particles, key=lambda p: p.best_value).position
    global_best_value = np.inf
    record = []

    plt.figure()
    plt.xlabel('Iterations')
    plt.ylabel('Best Fitness')
    plt.title('Best Fitness Evolution')
    
    for iter_num in range(num_iters):
        scores = objective_function_with_simulation(particles)
        for i, p in enumerate(particles):
            fitness = scores[i]
            if fitness < p.best_value:
                p.best_value = fitness
                p.best_position = p.position

        best_particle = min(particles, key=lambda p: p.best_value)
        if best_particle.best_value < global_best_value:
            global_best_value = best_particle.best_value
            global_best = best_particle.best_position

        record.append(global_best_value)

        plt.plot(record, color='b')
        plt.pause(0.01)
        
        print(f"Iteration {iter_num + 1}, Best value: {global_best_value}, Best position: {global_best}")

    plt.show()
    return global_best_value, global_best



best_value, best_position = PSO(num_particles, dim, num_iters, x_limits, v_limits)
print(f'Final Best value: {best_value}')
print(f'Final Best position: {best_position}')

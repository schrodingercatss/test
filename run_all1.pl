use strict;
use warnings;
use File::Copy qw(copy);
use File::Path qw(make_path);
use File::Spec;

# 定义模板目录和输入文件路径
my $origin_dir = "origin";
my $input_file = "from_PSO.data";

# 确保模板目录和输入文件存在
die "Template directory $origin_dir not found" unless -d $origin_dir;
die "Input file $input_file not found" unless -f $input_file;

# 删除旧的仿真目录
system("rm -rf sim_particle*");

foreach my $N (0..3) {
    # 创建目标目录
    my $current_dir = "sim_particle" . "$N";
    make_path($current_dir) or die "Failed to create directory $current_dir";

    # 复制模板目录内容到目标目录
    system("cp -rf $origin_dir/* $current_dir") == 0
        or die "Failed to copy contents from $origin_dir to $current_dir";

    foreach my $d (0..3) {
        # 打开输入文件并读取参数
        open my $fin, "<", $input_file or die "Cannot open $input_file: $!";
        my @values;

        while (<$fin>) {
            if ($. == $N + 1) {  # 查找对应行号
                chomp;
                @values = split;
                @values = reverse @values;
                last;
            }
        }
        close $fin;

        # 确保我们找到了需要的值
        if (scalar @values > $d) {
            my $pso_value = $values[$d];
            my $target_file = File::Spec->catfile($current_dir, "app_42com.sp");
            system("sed -i '/.temp 25/a.param pso_value$d = $pso_value' $target_file") == 0
                or die "Failed to update $target_file with value pso_value$d = $pso_value";
        }
    }

    # 使用绝对路径运行仿真脚本
    my $run_script_path = File::Spec->catfile($current_dir, "run_xa_vcs.sh");
    system("xterm -bg salmon -title \"sim_random_$N\" -e \"source $run_script_path | tee sim_random.log\" &") == 0
        or die "Failed to run simulation script $run_script_path";
}

use strict;
use warnings;
use Getopt::Long;
use Data::Dumper;

# 默认参数设置
my $num_simulations = 3;  # 默认模拟次数
my $num_dimensions = 4;   # 默认维度数量

# 从命令行读取参数
GetOptions(
    'num_simulations=i' => \$num_simulations,  # 接受一个整数类型的参数
    'num_dimensions=i'  => \$num_dimensions    # 接受一个整数类型的参数
) or die "Error in command line arguments\n";

# 检查模拟和维度数量
if ($num_simulations < 1 || $num_dimensions < 1) {
    die "Number of simulations and dimensions should be greater than 0\n";
}

print "Running with $num_simulations simulations and $num_dimensions dimensions\n";

system("rm -rf sim_particle*");

# 保存所有子进程的PID
my @child_pids;

foreach my $N (0..$num_simulations-1) {
    my $CURRENT_DIR = "sim_particle" . "$N";
    system("cp -rf origin $CURRENT_DIR");
    chdir("$CURRENT_DIR") or die "Cannot change directory to $CURRENT_DIR: $!";

    foreach my $d (0..$num_dimensions-1) {
        open my $FIN, "<", "../from_PSO.data" or die "Cannot open from_PSO.data: $!";
        my @values;
        while (<$FIN>) {
            if ($. == $N + 1) {  # 读取当前行编号
                chomp;
                @values = split;
                @values = reverse @values;
                last;
            }
        }
        close $FIN;

        if ($d < scalar @values) {
            my $pso_value = $values[$d];
            system("sed -i '/.temp 25/a.param pso_value$d = $pso_value' app_42com.sp");
        } else {
            warn "Dimension $d is out of bounds for simulation $N\n";
        }
    }

    system("xterm -bg salmon -title \"sim_random_$N\" -e \"source run_xa_vcs.sh | tee sim_random.log\" &");
    push @child_pids, $!;  # 保存子进程PID

    chdir("..") or die "Cannot change back to upper directory: $!";
}

# 等待所有子进程结束
foreach my $pid (@child_pids) {
    waitpid($pid, 0);
}